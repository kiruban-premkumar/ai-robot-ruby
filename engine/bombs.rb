class Bombs < Array
end

class Bomb
	attr_reader :id, :position, :essence, :power, :diameter, :exploded

	@@id = 0
	
	def initialize(position)

		@position = Position.new(position.x, position.y, 0)
		@position.angle = position.angle
		@position.x += 32*Math.cos(@position.angle)
		@position.y += 32*Math.sin(@position.angle)
		
		
		@id=@@id
		@@id+=1
		
		@essence = 200
		@power = 2
		@diameter = 0
		
		@exploded = false
	end
	
	def move
		@position.x += 1.5 * Math.cos(@position.angle)
		@position.y += 1.5 * Math.sin(@position.angle)
		@essence -= 1
			
		if @essence <= 0
			@exploded = true
		end
	end
	
	def touch
		if @exploded == false
			@exploded = true
			@power += @essence/400
		end
	end
	
	def explode
		@diameter += @power
		@power -= 0.02
	end
end
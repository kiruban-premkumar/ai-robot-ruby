# Position d'un object

class Position
	attr_accessor :x, :y, :angle
	
	def initialize(x,y,angle)
		@x, @y = x, y
		@angle = angle * Math::PI/180
	end
end
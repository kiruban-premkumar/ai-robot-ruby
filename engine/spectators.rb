class Spectators < Array

	def send(data, client = nil)
		close
		each do |spectator|
			if spectator != client
				spectator.send(JSON data)
			end
		end
	end

	# Remove closed connections
	def close
		each do |spectator|
			delete(spectator) if spectator.state == :closed 
		end
	end
end
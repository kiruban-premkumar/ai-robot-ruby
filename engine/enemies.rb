require 'playable'
require 'missiles'

class Enemies < Array
	
	def initialize(id)
		@id = id
	end
	
	def find(id)
		item = select {|enemy| enemy.id == id }
		item.first
	end

	def count(dead)
		i=0
		each do |enemy|
			if enemy.id != @id and (enemy.life > 0 or dead)
				i+=1
			end
		end
		i
	end
	
	def list(dead, &block)
		if block_given?
			each do |enemy|
				if enemy.id != @id and (enemy.life > 0 or dead)
					yield enemy
				end
			end
		else
			data = Array.new
			each do |enemy|
				if enemy.id != @id and (enemy.life > 0 or dead)
					data.push(enemy)
				end
			end
			data
		end
	end

	def bombs
		i = Array.new
		each do |bot|
			bot.bombs.each do |bomb|
				i.push(Missile.new(bomb))
			end
		end
		i.flatten
	end
end

class Enemy < Playable
	attr_reader :bombs
	
	def id
		@object.id
	end

	def life
		@object.life
	end
	
	def bombs
		@object.bombs
	end
end
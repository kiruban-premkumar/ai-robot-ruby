class Playable
	
	def initialize(object)
		@object = object
	end
	
	def x
		@object.position.x
	end
	
	def y
		@object.position.y
	end
	
	def angle
		angle = @object.position.angle / Math::PI*180
		angle += 360 if angle < 0
		angle.round
	end
	
	def angleTo(object)
		angle = Math.atan2((object.y - self.y),(object.x - self.x))
		angle -= @object.position.angle

		angle = (angle/Math::PI*180).round
		angle -= 360 if angle > 180
		angle
	end
	
	def distance(object)
		dx = object.x - self.x
		dy = object.y - self.y
		
		((dx**2)+(dy**2))**0.5
	end
end
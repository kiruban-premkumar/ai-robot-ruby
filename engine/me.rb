require 'playable'

class Me < Playable
	attr_reader :data, :enemies

	def initialize(object, enemies)
		@data = Hash.new
		@enemies = enemies
		@bombs = Bombs.new
		super(object)
		self.init
	end

	def init

	end
	
	def id
		@object.id
	end
	
	def life
		@object.life
	end
	
	def move!(dir)
		@object.move(dir)
	end
	
	def turn!(dir)
		@object.turn(dir)
	end
	
	def fire!
		@object.fire
	end

	def log(message)
		
	end
	
	def detect(objects, angle, distance)
		detected = Array.new
				
		objects.each do |object|
			if angleTo(object).between?(-angle/2, angle/2) and distance > distance(object) and object != self
				detected.push(object)
			end
		end
		detected
	end

	def bombs
		data = Array.new
		@object.bombs.each do |bomb|
			data.push(Missile.new(bomb))
		end
		data.flatten
	end

	def recharge
		@object.recharge
	end

	def temperature
		@object.warm
	end
end
require 'config'

require 'singleton'
require 'rubygems'
require 'em-websocket'
require 'json'

require 'matchs'

class Server
	attr_reader :matchs
	
	include Singleton

	@@available = ["createMatch","matchsList","addSpectator","chat"]

	def initialize
		@matchs = Matchs.new # Table contened matchs
	end
	
	# Create a new matchs
	def createMatch(bots, *data)
		@matchs.push(Match.new(bots))
	end
	
	# Add spectator to a match
	def addSpectator(id, client)
		JSON @matchs.find(id).addSpectator(client)
	end
	
	def matchsList(*data)
		JSON @matchs.list
	end
	
	def chat(params, client)
		@matchs.find(params["idMatch"]).chat(params["value"], client)
	end


	def listen
		# For display matches to clients
		EventMachine.run do

		    EventMachine::WebSocket.start(:host => Config::Server["match"]["host"], :port => Config::Server["match"]["port"]) do |wSocket|
		        
				# To do when a new connection open
				wSocket.onopen do
		          puts "[Info] New client connection open"
		        end

				# To do when a connection close
		        wSocket.onclose do
					puts "[Info] Connection closed"
				end

				# Parse les requetes JSON et execute la fonction correspondante
		        wSocket.onmessage do |msg|
					puts "[Info] Recieved: #{msg}"
					
					begin
						data = JSON.parse msg
						
						# Verify message
						if @@available.index(data['Method'].to_s).nil?
							raise "Unknown server function #{data['Method'].to_s}"
						end
						
						callback = self.send(data['Method'].to_sym, data['Params'], wSocket)
						if callback
							wSocket.send(callback)
						end
					rescue Exception => e
						# Display error message
						puts "[Error] #{e.message}"
						wSocket.close_connection
					end
		        end
		    end
		end
	end
end
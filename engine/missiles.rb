require 'playable'
require 'bombs'

class Missiles < Array
end

class Missile < Playable
	def diameter
		@object.diameter
	end
	
	def exploded?
		@object.exploded
	end
end
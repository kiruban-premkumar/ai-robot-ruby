module Config

	Server = {
		"match" => {
			"host" => "0.0.0.0",
			"port" => 8080,
		},

		"sandbox" => {
			"host" => "0.0.0.0",
			"port" => 8081,
		},
	}

end
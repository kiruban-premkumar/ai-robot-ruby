require 'direction'
require 'me'
require 'missiles'

include Direction

# L'algorithme du joueur est encapsulé dans cette classe
class Runnable
	attr_reader :me, :enemies, :process

	def initialize(bot, enemies, process)
		@me = Me.new(bot)
		@enemies = enemies
		@bombs = Bombs.new
		
		@process = process
	end
	
	# Execute l'algorithme
	def run
		if @me.life > 0 and @enemies.count(false) > 0
			eval(@process)
		end
	end
end
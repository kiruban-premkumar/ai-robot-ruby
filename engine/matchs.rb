require 'spectators'
require 'bots'
require 'positions'

require "uri"
require "net/http"


# Tableau de tous les matchs
class Matchs < Array

	# Rechercher un match
	def find(id)
		item = select {|match| match.id.to_s == id }
		item.first
	end

	def push(match)
		super(match)
		match.id
	end

	def list
		data = Array.new
		each do |match|
			bots = Hash.new
			match.bots.each do |bot|
				bots[bot.idDB] = bot.name
			end

			data.push({:id => match.id.to_s, :bots => bots, :spectators => match.spectators.count, :date => match.date.to_s})
		end
		data
	end
end

class Match
	attr_reader :id, :spectators, :bots, :running, :rank, :message, :date
	
	@@id = 1
	@@duration = 10 # Durée max du match en minutes

	# Définition des positions initiales des bots
	@@positions = [
		Position.new(42,42,85),
		Position.new(410,42,90),
		Position.new(758,42,135),
		Position.new(42,558,-45),
		Position.new(410,558,-90),
		Position.new(758,558,-135)
	]

	@@fps = 60 # Cadence du jeu

	def initialize(bots)
		@id = @@id
		@@id+=1

		puts "[info] : new match with id #{@id}"
		
		@date = Time.now.to_i

		@bots = Bots.new
		@spectators = Spectators.new
		
		@rank = Hash.new

		@status = 0
		
		ennemies = Array.new


		# Initialisation des bots
		positions = @@positions.shuffle
		bots.each do |botData|
			if positions.count > 0
				bot = Bot.new(botData,positions.slice!(0).clone,self)
				ennemies.push(Enemy.new(bot))
				@bots.push(bot)
			end
		end
		
		# Création pour chaque bots une variable ennemies qui contient tout les bots
		@bots.collect do |bot|
			bot.setRunnable(bot.id, ennemies)
		end
	

		@running = true
		@open = true
		
		start # Démarre le match
	end
	
	# Ajout d'un spéctateur au match
	def addSpectator(client)
		puts "[info] new spectators in match #{@id}"
		@spectators.push(client)
		
		# Retourne nom du match
		{"Method"=>"name", "Message"=>@id}
	end

	def allPlayable
		[@bots,@bots.bombs].flatten
	end
	
	# Chat entre les joueurs
	def chat(data, client)
		msg = {}
		
		msg["Method"] = "chat"
		msg["Message"] = data
		
		#@spectators.send(JSON msg, client)
		
		@spectators.each do |spectator|
			if spectator != client
				spectator.send(JSON msg)
			end
		end
	end
	
	def start
		# Chaque match est executé dans un thread séparé
		Thread.new do
			while @open
				@spectators.send(@bots.data(@message)) # Envoi aux joueurs du rendu
				sleep(1.0/@@fps)
			end
		end

		Thread.new do
			@message = 'Chargement...'
			sleep(5)
			@status = 1
			begin
				@message = nil
				i = 0
				
				# Boucle du match
				while @running
					
					start = Time.now
					
					
					@bots.run # Execution de chaque bots

					@bots.each do |bot|
						if bot.life <= 0 and !bot.dead
							bot.dead = true
							@rank[@rank.size] = bot.idDB
						end
					end
							
					
					
					i+=1
					
					# Temps d'attente avant la nouvelle execution
					time = (1.0/@@fps)-(Time.now-start)
					time = 0 if time < 0
					sleep(time)
					
					# Condition de sortie de la boucle
					if i >= @@duration*3600 or @bots.count <= 1
						@running = false 
					end
				end
				
				close # Termine le match
				
			rescue Exception => e 
				puts "[Error] #{e.message}"
				close
			end  
		end #Thread
	end
	
	private
	
	# Ferme la connexion
	def close
		@message = 'Fin du match'
		sleep(2)
		@bots.sort!{|a,b| a.life <=> b.life}.each do |bot|
			if !bot.dead
				bot.dead = true
				@rank[@rank.size] = bot.idDB
			end
		end

		params = Hash.new
		params['rank'] = @rank

		x =  Net::HTTP.post_form(URI('http://dev.k42.fr/match/rank'),'rank'=> JSON(@rank))
		#x =  Net::HTTP.post_form(URI('http://localhost/match/rank'),'rank'=> JSON(@rank))
		@status = 2
		
		sleep(5)
		@open = false
		# Fermeture de la connection de tout les spectateurs
		@spectators.each do|spectator|
			spectator.close_connection
		end
		Server.instance.matchs.delete(self) # Delete match
	end
end
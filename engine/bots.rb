require 'positions'
require 'enemies'
require 'bombs'
require 'runnable'
require 'me'

require 'timeout'

# Tableau de tous les bots
class Bots < Array
	# Rechercher un bot
	def find(id)
		item = select{|bot| bot.id == id}
		item.first
	end
	
	def bombs
		i = Array.new
		each do |bot|
			i.push(bot.bombs)
		end
		i.flatten
	end

	def run
		each do|bot|
			bot.bombsCollision
			bot.moveBombs

			if !bot.thr or !bot.thr.status 
				bot.thr = Thread.new do
					Timeout::timeout(1) do
					    bot.run
					end
				end
			end
		end
	end

	def data(message = nil)
		data = {}
		data['bots'] = Hash.new
		data['bombs'] = Array.new

		each do|bot|
			botName = "bot#{bot.id}"
			data['bots'][botName] = {}
			data['bots'][botName]["x"] = bot.position.x
			data['bots'][botName]["y"] = bot.position.y
			data['bots'][botName]["angle"] = bot.position.angle
			data['bots'][botName]["life"] = bot.life
			data['bots'][botName]["id"] = bot.id
			data['bots'][botName]["name"] = bot.name
		
		
			bot.bombs.each do|bomb|
				if !bomb.nil?
			
					item = {}
					item["y"] = bomb.position.y
					item["x"] = bomb.position.x
					item["angle"] = bomb.position.angle
					item["exploded"] = bomb.exploded
					item["diameter"] = bomb.diameter
					
					data['bombs'].push(item)
				end
			end
		end

		if message
			data['msg'] = message
		end	
		data
	end

	# Nombre de bots en vie
	def count
		number = 0
		each do |bot|
			if bot.life > 0
				number += 1
			end
		end
		number
	end
end


class Bot
	attr_reader :id, :life, :name, :position, :idDB, :bombs, :warm, :recharge
	attr_accessor :dead, :thr

	@@id = 0

	@@direction = {:left => -1, :right => 1, :front => 1, :back => -1}
	
	def initialize(data, position, match)
		puts "[info] : initialized bots #{data['ID']}"
		
		@id = @@id
		@@id += 1
		@idDB = data['ID']
		
		@bombs = Bombs.new

		@match = match
		
		@position = position
		@life = 1000
		@warm = @recharge = 0

		@hasMove = @hasTurn = @hasFire = false
		
		@name = data['Name']
		@process = data['Algorithm']

		@dead = false
	end

	# Deplacement
	def move(dir)
		if !@hasMove
			if !xCollision(dir)
				@position.x += @@direction[dir]*Math.cos(@position.angle)
			else
				@life -= Math.cos(@position.angle).abs
			end
			
			if !yCollision(dir)
				@position.y += @@direction[dir]*Math.sin(@position.angle)
			else
				@life -= Math.sin(@position.angle).abs
			end
			@hasMove = true
		end
	end

	# Rotation
	def turn(dir)
		if !@hasTurn

			@position.angle += @@direction[dir] * Math::PI/180
			
			@position.angle -= 2*Math::PI if @position.angle > 2*Math::PI
			@position.angle += 2*Math::PI if @position.angle < -2*Math::PI
			
			@hasTurn = true
		end
	end

	# Tir
	def fire
		if !@hasFire
			if @recharge == 0
				@bombs.push(Bomb.new(@position))
				@warm += 180
				@recharge = 60
			end
			@hasFire = true
		end
	end


	# Pour protéger l'objet
	def setRunnable(id, items)
		


		begin
			moduleName = "Match#{@match.id}Bot#{id}"

			eval <<-EOC
				module #{moduleName}
					#{@process}
				end
			EOC

			enemies = Enemies.new(@id)
			enemies.concat(items)

			##@runnable = Runnable.new(self, enemies, @process)
			
			@runnable = eval(moduleName)::Player.new(self, enemies)
		rescue Exception => e
			# Affiche le message d'erreur et ferme la connection
			puts "[Error] #{e.message}"
			@life = 0
		end
	end
	
	# Execute l'algorithme
	def run
		
		update
		
		begin
			if life > 0 and @runnable.enemies.count(false) > 0
				@runnable.run
			end
		rescue Exception => e 
			puts "[Error] bot discalified : #{e.message}"
			# En cas d'erreur, on discalifie le bot
			@life = 0
		end
	end


	def moveBombs
		# Deplacement des bombs
		@bombs.each do |bomb|
			
			#collision entre les objets
			@match.allPlayable.each do |object|
				if bomb != object and self != object
				
					dx = bomb.position.x - object.position.x
					dy = bomb.position.y - object.position.y
					
					if ((dx**2)+(dy**2))**0.5 < 32
						bomb.touch
					end
					
				end
			end

			if bomb.exploded
				bomb.explode
				
				if bomb.diameter <= 0
					@bombs.delete(bomb)
				end
			else
				bomb.move
			end
		end
	end


	def update

		# Update bot data
		@life -= 1 if @warm > 600
		@warm -= 1 if @warm > 0
		@recharge -= 1 if @recharge > 0
		
		# Reset actions
		@hasMove = @hasTurn = @hasFire = false
	end
	
	def xCollision(dir)
	
		# Colision avec les ennemis
		@runnable.enemies.list(true).each do|enemy|

			dx = (@position.x + @@direction[dir]*Math.cos(@position.angle)) - enemy.x
			dy = @position.y - enemy.y
		
			if ((dx**2)+(dy**2))**0.5 <= 64
				return true
			end
		end
		
		#Colision avec les murs
		if @position.x + @@direction[dir]*Math.cos(@position.angle) - 32 < 0 or @position.x + @@direction[dir]*Math.cos(@position.angle) + 32 > 800
			return true
		end
		
		return false
	end
	
	def yCollision(dir)
	
		#Colision avec les ennemis
		@runnable.enemies.list(true).each do|enemy|
			
			dx = @position.x - enemy.x
			dy = (@position.y + @@direction[dir]*Math.sin(@position.angle)) - enemy.y
			
			if ((dx**2)+(dy**2))**0.5 <= 64
				return true
			end
		end
		
		#Colision avec les murs
		if @position.y + @@direction[dir]*Math.sin(@position.angle) - 32 < 0 or @position.y + @@direction[dir]*Math.sin(@position.angle) + 32 > 600
			return true
		end
		
		return false
	end

	# Colision avec les bombs
	def bombsCollision
		@match.bots.bombs.each do |bomb|
			
			dx = @position.x - bomb.position.x
			dy = @position.y - bomb.position.y
			distance = ((dx**2)+(dy**2))**0.5
			
			if distance < 48 + bomb.diameter and bomb.exploded
				@life -= 1
			end
		end
	end
end